var canvas;
var context;
var screenH;
var screenW;
var flakes = [];
var fps = 30;
var maxSize = 10;
var maxFlakes = 100;
var minFlakes = maxFlakes*0.5;
var env = null;
var startTime = new Date();

// from https://gist.github.com/A973C/3856600

$('document').ready(function () {
	// Calculate the screen size
	screenH = $(window).height();
	screenW = $(window).width();

	env = new Environment();

	// Get the canvas
	canvas = $('#space');

	// Fill out the canvas
	canvas.attr('height', screenH);
	canvas.attr('width', screenW);
	context = canvas[0].getContext('2d');

	// Create all the stars
	for (var i = 0; i < clamp(maxFlakes * Math.random(), minFlakes, maxFlakes); i++) {
		// Create a new star and draw
		let flake = new Flake(env);
		flake.setupInitialPosition();
		flakes.push(flake);
	}


	setInterval(animate, 1000 / fps);
});


class AudioFlake {
	constructor(url) {
		this.audio = [];
		this.url = url;

		this.playExtents = {
			size: 					[100000, 0],
			moveSusceptibility0: 	[100000, 0],
			moveSusceptibility1: 	[100000, 0],
		};
	}

	createNew() {
		let a = new Audio(this.url);
		a.mozPreservesPitch = false;
		this.audio.push(a);
		return a;
	}

	get() {
		let slot = this.audio.filter((a) => {return a.ended;})[0];
		if (!slot) {
			if (this.audio.length<5) {
				slot = this.createNew();
			}
			else {
				slot = null;
			}
		}

		return slot;
	}

	play(size, opacity, moveSusceptibility) {
		this.playExtents.size[0] = Math.min(this.playExtents.size[0], size);
		this.playExtents.size[1] = Math.max(this.playExtents.size[1], size);

		this.playExtents.moveSusceptibility0[0] = Math.min(this.playExtents.moveSusceptibility0[0], moveSusceptibility[0]);
		this.playExtents.moveSusceptibility0[1] = Math.max(this.playExtents.moveSusceptibility0[1], moveSusceptibility[0]);

		this.playExtents.moveSusceptibility1[0] = Math.min(this.playExtents.moveSusceptibility1[0], moveSusceptibility[1]);
		this.playExtents.moveSusceptibility1[1] = Math.max(this.playExtents.moveSusceptibility1[1], moveSusceptibility[1]);

		let a = this.get();

		if (a) {
			// a.volume = clamp(
			// 	moveSusceptibility[0]/this.playExtents.moveSusceptibility0[1],
			// 	0.25, 1);

			a.volume = clamp(opacity, 0.1, 1);

			a.playbackRate = clamp(
				4*(this.playExtents.size[1] - size)/this.playExtents.size[1],
				0.5, 4);
			a.play();
		}

		this.playExtents.size[0]*= 1.02;
		this.playExtents.size[1]*= 0.98;

		this.playExtents.moveSusceptibility0[0]*= 1.02;
		this.playExtents.moveSusceptibility0[1]*= 0.98;

		this.playExtents.moveSusceptibility1[0]*= 1.02;
		this.playExtents.moveSusceptibility1[1]*= 0.98;
	}
}


var audio = {
	bell: new AudioFlake("audio/bell2.wav"),
	maxBell: 0,
	minBell: 10000,
}


/**
 * Animate the canvas
 */
animate = function() {
	context.clearRect(0, 0, screenW, screenH);

	env.move();

	aliveFlakes = 0;
	movingFlakes = 0;
	for (let i=0; i<flakes.length; i++) {
		flakes[i].move().draw();
		aliveFlakes+= flakes[i].alive;
		movingFlakes+= (flakes[i].alive && flakes[i].allowMove);
	}

	if (aliveFlakes<maxFlakes) {
		for (let i = 0; i < (maxFlakes-minFlakes) * Math.random()*0.1; i++) {
			// Create a new star and draw
			let flake = new Flake(env);
			flake.setupInitialPosition();
			flake.setupTopPosition();
			flakes.push(flake);
		}
	}

	if (movingFlakes<(minFlakes*0.5)) {
		if (env.hasTransitioned()) {
			let nt = 5*(Math.random()-0.1);
			console.info('temp, hot', nt);
			env.setTemperature(nt);
		}
	}
	else if (movingFlakes>(aliveFlakes*0.9)) {
		if (env.hasTransitioned()) {
			let nt = 5*(Math.random()-0.85)-5;
			console.info('temp, freeze', nt);
			env.setTemperature(nt);
		}
	}
	else {
		if (Math.random()<0.1 && env.hasTransitioned()) {
			let nt = 5*(Math.random()-0.7);
			console.info('temp', nt);
			env.setTemperature(nt);
		}

		if (Math.random()<0.1 && env.hasTransitioned()) {
			let nw = [
				25*(Math.random()-0.5),
				7*(Math.random()-0.5),
			];
			console.info('windy', nw);
			env.setWindDirection(nw);
		}
	}

	// cleanup dead flakes
	flakes = flakes.filter((f) => {
		return f.alive;
	});

	env.stats.movingFlakes = movingFlakes;
	env.stats.aliveFlakes = aliveFlakes;
	env.stats.flakes = flakes.length;
}

class Environment {
	constructor() {
		this.windDirection = [0, 0,];
		this.gravity = [0, 2];
		this.temperature = 0.1;

		this.state = {
			windDirection: this.windDirection,
			temperature: this.temperature,
		};

		this.alive = true;
		this.stats = {
			flakes: null,
			movingFlakes: null,
			aliveFlakes: null,
		};
	}

	hasTransitioned() {
		if (!this.alive) {
			return false;
		}

		return     Math.abs(this.windDirection[0] - this.state.windDirection[0])<0.000001
				&& Math.abs(this.windDirection[1] - this.state.windDirection[1])<0.000001
				&& Math.abs(this.temperature - this.state.temperature)          <0.00000001;
	}

	setWindDirection(windDirection) {
		this.state.windDirection = windDirection;
		return this;
	}

	setTemperature(temperature) {
		this.state.temperature = temperature;
		return this;
	}

	move() {
		if (!this.alive) {
			return this;
		}
		this.windDirection[0] = (19*this.windDirection[0] + 1*this.state.windDirection[0])/20;
		this.windDirection[1] = (19*this.windDirection[1] + 1*this.state.windDirection[1])/20;
		this.temperature = 		(19*this.temperature + 1*this.state.temperature)/20;
		return this;
	}
}

clamp = function (a, min, max) {
	return Math.max(Math.min(max, a), min);
}

class Flake {
	x = null;
	y = null;
	angle = null;
	opacity = null;
	env = null;
	allowMove = false;
	moveSusceptibility = [1, 1];
	temperatureSusceptibility = 0;
	color = [255, 255, 200];

	constructor(env) {
		this.env = env;
		this.alive = true;

		this.color = [
			(220+Math.random()*(255-220)),
			(220+Math.random()*(255-220)),
			(220+Math.random()*(255-220)),
		];
	}

	setupInitialPosition() {
		this.x = Math.random() * screenW;
		this.y = Math.random() * screenH;

		this.size = Math.random() * maxSize;

		this.opacity = Math.random() * 1;

		this.moveSusceptibility = [
			clamp(((maxSize-this.size)/maxSize)*Math.random(), 0.01, 1),
			clamp(((maxSize-this.size)/maxSize)*Math.random()*Math.random(), 0.25, 1),
		];
		this.temperatureSusceptibility = Math.random() * 0.050;

		this.rotationAngle = (maxSize-this.size) * (Math.random() - 0.5) * 0.1;
		this.angle = 0;
		this.allowMove = true;

		return this;
	}

	setupTopPosition() {
		this.setupInitialPosition();

		this.y = 0;

		return this;
	}

	move() {
		if (!this.alive) {
			return this;
		}
		if (!this.env.alive) {
			return this;
		}

		if (this.allowMove) {
			this.x+= (this.env.windDirection[0])*this.moveSusceptibility[0] + this.env.gravity[0];
			this.y+= (this.env.windDirection[1])*this.moveSusceptibility[1] + this.env.gravity[1];
			this.angle+= this.rotationAngle;
			this.size-= (this.env.temperature>0 ? this.env.temperature*this.temperatureSusceptibility : this.temperatureSusceptibility*2);

			this.opacity-= clamp(((maxSize-this.size) * this.env.temperature) * 0.0005, 0.00001, 0.10);
			this.rotationAngle*= 0.995;
		}
		else {
			this.opacity-= clamp(((maxSize-this.size) * this.env.temperature) * 0.0050, 0.0010, 0.10);
		}

		if (this.allowMove && (this.y+this.size)>screenH) {
			this.allowMove = false;

			if (this.size>1 && this.opacity>0.01) {
				audio.bell.play(this.size, this.opacity, this.moveSusceptibility);
			}
		}

		if (this.opacity<0.001) {
			this.allowMove = false;
		}

		if (this.opacity<0.0 || this.size<0.0) {
			this.alive = false;
		}
		if (this.x>screenW) {
			this.alive = false;
		}
		if (this.x<0) {
			this.alive = false;
		}

		return this;
	}

	draw() {
		if (!this.alive) {
			return this;
		}

		// Save the context
		context.save();

		// move into the middle of the canvas, just to make room
		context.translate(this.x, this.y);
		context.rotate(this.angle);
		context.beginPath();
		for (let i = 5; i--;) {
			context.lineTo(0, this.size);
			context.translate(0, this.size);
			context.rotate((Math.PI * 2 / 10));
			context.lineTo(0, -this.size);
			context.translate(0, -this.size);
			context.rotate(-(Math.PI * 6 / 10));
		}
		context.lineTo(0, this.size);

		context.closePath();
		context.fillStyle = "rgba("+this.color[0]+", "+this.color[0]+", "+this.color[0]+", " + this.opacity + ")";
		context.shadowBlur = 5;
		context.shadowColor = '#ffff33';

		context.fill();

		context.restore();

		return this;
	}
}
